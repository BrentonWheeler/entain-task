## Steps to run application
`npm install ; npm run start`\
or \
`yarn ; yarn start`

The application should then be available at http://localhost:9000

### Assumptions
- Design quality wasn't a priority 
- I was creating an application that was going to be used long term (so abstractions aren't a waste of time)
- With the specification of "A user should see 5 races at all times" with the default api request url given of https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=10 this wouldn't have been posible when given the next requirement of "User should be able to toggle race categories to view races belonging to only the selected category" would leave less than 5 per category. Because I couldn't guess the api request to get the next 5 races for a given category, I opted for just using `count=100`, while this could still return less than 5 for one of the 3 categories, it makes it far less likely which was the compromise I've gone with. NOTE: this is not an ideal solution, Ideally I would have communicated my issue with a backend dev and/or had the API specification to find a more performant solution rather than races more races than I needed.
- Outside of the above case, where I could I've tried to make this performant for the API eg:
in `/src/features/races/races.js` I've used intervals, ensuring we only check if the races currently in the store have become stale once a minute, and then not blindly requesting once a minute regardless, by confirming some races actually started over 1 minute ago before requesting new races

### Approach
- I wanted to showcase React knowledge here as a priority so I went for a slightly over engineered solution using hooks, context and render props in this case. The benefit of this setup is that it's very scalable, for instance you could copy paste the JSX  `<TabsProvider>...</TabsProvider>` several times in the same component and they will play nicely together with no extra configuration. This setup also hides a lot of state management inside components or hooks that the state is relevant to, so that higher level components can be as clean and readable as possible for reference in this case view `/src/screens/next-to-go.js`
- Secondly I wanted to use Redux to show knowledge but also out of some nessecity - to start with for an application of this scope it never feels like a requirement but often gets that way quickly and suddenly
  - NOTE: I haven't used raw redux in a while, there is room for refactoring the initial setup, from here I would look into Redux toolkit (haven't used before) or a similar library to reduce boilerplate of Redux

### Next steps
- Refactor the `features/races/*` files, the reason these haven't been created optimally from the get go has been trying to stay somewhat faithful to the time constraints (although I probably still spent about 5 hours on this so I had to cut something)
  - The decision to cut the code quality of these files was due to them being the highest level of domain specificity, having poor abstractions that other people begin to use would create a much worse outcome than having poor quality in the top level component that utilises abstractions (aka races)
  - Refactoring in this case would be things like pulling out the hooks into a `utils` folder where they could have more code reuse (where it makes sense)
  - Using functions rather than inlining variable defininations
  - And generally cleaning up logic
- Redux abstration
- Typescript
- Unit testing
- Storybook to show component expected useages and configurations at a glance for other dev's DX

### Left out
- I orginally wanted to include typescript types and unit testing but as I'm rusty in these area's I opted to avoid them as it would've blown out the time allocated for this task
- I also took a fairly fast approach to styling here too for the same reason, although with that decision tailwind was very intentional, as it's extremely fast and easy to prototype solutions with
- Documentation, maybe questionably, my general approach (although I'm open to changing this based on team requirements) is to only add comments where code becomes complex, and hopefully the code is clean enough that it's self documenting otherwise (and where it's conceptually difficult to understand reasoning, having a seperate living documentation in something like notion for example)
  - That being said, I did add more comments generally to the `features/races/*` files as they were the most rushed and will need refactoring, refactoring poor code is always made easier with indepth comments on the behaviour and intention behind the implementation - so I made it a point to comment this code as a bare minimum requirement