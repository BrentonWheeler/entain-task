import React, {
  createContext,
  useMemo,
  useContext,
  useState
} from 'react';

export const TabsContext = createContext({});

export function TabsProvider({ children }) {
  const [selectedTab, setSelectedTab] = useState(null);
  const value = useMemo(() => ({ selectedTab, setSelectedTab }), [selectedTab]);

  return (
    <TabsContext.Provider value={value}>
        <ol className="m-16 max-w-full flex flex-col" >
            {children}
        </ol>
    </TabsContext.Provider>
  );
}

export const useTabs = () => {
  const tabs = useContext(TabsContext);

  if (tabs?.selectedTab === undefined) {
    throw new Error('The tabs context was accessed outside of the provider tree');
  }

  return tabs;
};