import React from 'react'
import { useTabs } from './tabs'

function TabContent ({ children }) {
    const { selectedTab } = useTabs();

    return (
        <div>
            {children({selectedTab})}
        </div>
    )
}

export { TabContent };