import React, { useEffect } from 'react'
import { useTabs } from './tabs';

function Tab ({ children, id, isDefault }) {
    const { selectedTab, setSelectedTab } = useTabs();

    useEffect(() => {
        if (isDefault) {
            setSelectedTab(id);
        }  
    }, [])

    return (
        <button onClick={() => setSelectedTab(selectedTab !== id ? id : null)} className={`p-4 m-4 rounded filter hover:drop-shadow-md active:bg-pink-500 transform duration-200 cursor-pointer ${selectedTab === id ? 'drop-shadow-md bg-red-300 hover:bg-red-400 ' : 'bg-pink-300 hover:bg-pink-400 '}`}>
            {children}
        </button>
    )
}

export { Tab };