import { TabsProvider, useTabs } from './tabs';
import { Tab } from './tab';
import { TabContent } from './tab-content';

export { TabsProvider, Tab, TabContent, useTabs };