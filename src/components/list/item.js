import React from 'react'

function ListItem ({children}) {
    return (
        <div className="p-4 m-2 border rounded filter hover:drop-shadow-md">
            {children}
        </div>
    )
}

export { ListItem };