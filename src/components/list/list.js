import React from 'react'

function List ({children}) {
    return (
        <ol>
            {children}
        </ol>
    )
}

export { List };