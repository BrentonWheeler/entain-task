import React from 'react'
import ReactDOM from 'react-dom'
import './styles.css'
import { Provider } from 'react-redux'
import { store } from 'data'
import { Layout, NextToGoScreen } from 'screens'

function App () {
    return (
        <Provider store={store}>
            <Layout>
                <NextToGoScreen />
            </Layout>
        </Provider>
    )
}

ReactDOM.render(
    App(),
    document.getElementById('app')
)