import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { store } from 'data'
import { TabsProvider, Tab, TabContent } from 'components/tabs'
import { Races } from 'features/races'
import { getRaces } from 'data/races'

function NextToGoScreen ({races}) {
    useEffect(() => {
        store.dispatch(getRaces)
    }, [])
    
    return (
        <>
            <h1 className="text-center text-5xl py-8">Next to go</h1>
            <TabsProvider>
                <div className="flex justify-center bg-gray-200 m-2">
                    <Tab id='9daef0d7-bf3c-4f50-921d-8e818c60fe61'>Greyhound racing</Tab>
                    <Tab id='161d9be2-e909-4326-8c2c-35ed71fb460b'>Harness racing</Tab>
                    <Tab id='4a2788f8-e825-4d36-9894-efd4baf1cfae'>Horse racing</Tab>
                </div>
                <TabContent>
                    {(tabs) => <Races races={races?.data} selectedCategory={tabs.selectedTab}/>}
                </TabContent>
            </TabsProvider>
        </>
    )
}

const mapStateToProps = state => {
    return {
        races: state.races,
    };
};

export default connect(mapStateToProps)(NextToGoScreen);