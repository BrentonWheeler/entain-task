import React from 'react'
import NextToGoScreen from "./next-to-go";

const Layout = ({children}) => {
    return (
        <div className="max-w-full md:container md:mx-auto">
            {children}
        </div>
    );
}

export { Layout, NextToGoScreen };