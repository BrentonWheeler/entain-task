import React from 'react'
import { ListItem } from 'components/list';

import dayjs from 'dayjs'

// NOTE: this function converts negative/positive seconds to positive minutes and seconds, to keep things simple
const secondsToMinutes = (secondsFromStart) => {
    let result = '';
    if (Math.abs(secondsFromStart) > 60) {
        result+= `${Math.abs(Math.trunc(secondsFromStart / 60))}m `
    }
    return result+= `${Math.abs(secondsFromStart % 60)}s`
}

function Race ({race, currentTime}) {
    const { meeting_name, race_number, advertised_start } = race;
    const secondsFromStart = dayjs().diff(dayjs.unix(advertised_start?.seconds), 'second');
    const counter = secondsToMinutes(secondsFromStart)
    // racePassed is used to style and add a minus sign before the counter if true
    const racePassed = advertised_start?.seconds < currentTime;

    return (
        <ListItem>
            <span className="flex justify-between">
                <div className="flex flex-col lg:flex-row gap-2">
                    <span><b>Meeting name:</b> {meeting_name},</span>
                    <span><b>Race number:</b> {race_number}</span>
                </div>
                <span className={`${racePassed ? 'text-red-500' : ''}`}>{racePassed ? '-' : ''}{counter}</span>
            </span>
        </ListItem>
    );
}

export { Race };
