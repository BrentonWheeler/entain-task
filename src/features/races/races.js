import React, { useEffect, useState } from 'react'
import { List } from 'components/list';
import { Race } from './race'
import { connect } from 'react-redux'
import { store } from 'data'
import { getRaces } from 'data/races'
import { useInterval } from 'hooks/use-interval'
import dayjs from 'dayjs'

const MAX_RACES_SHOWN = 5;

function Races ({ races, selectedCategory }) {
    const [currentTime, setCurrentTime ] = useState();
    const [shownRaces, setShownRaces ] = useState();

    useInterval(() => {
        setCurrentTime(dayjs().unix())
    }, 1000)

    useInterval(() => {
        // This checks once a minute if we have any races that ran over a minute ago (to not over hit the API)
        // at which point, if it does it will request 10 new races via redux
        const hasFreshRaces = races.next_to_go_ids.every((raceId) => {
            return races.race_summaries[raceId].advertised_start.seconds > currentTime - 60 
        })

        if(!hasFreshRaces){
            store.dispatch(getRaces)
        }
    }, 60000)

    useEffect(() => {
        if(!races?.next_to_go_ids){
            return
        }

        // This hides any races that have run more than 1 minute ago AND limit's the max shown races to `MAX_RACES_SHOWN` (5)
        // It also hides any races that are not of the selected race category, if one has been selected
        const filteredRaces = races.next_to_go_ids.filter((raceId) => {
            if (selectedCategory && selectedCategory !== races.race_summaries[raceId].category_id) {
                return false
            }
            if (races.race_summaries[raceId].advertised_start.seconds < currentTime - 60) {
                return false
            }
            return true
        }).slice(0, MAX_RACES_SHOWN);
        setShownRaces(filteredRaces);

    }, [currentTime, selectedCategory])


    // TODO: refactor this and create a loading state abstraction
    if(!races?.next_to_go_ids){
        return <span>loading...</span>
    }

    return (
        <List>
            {shownRaces?.map((raceId) => <Race key={raceId} race={races.race_summaries[raceId]} currentTime={currentTime} />)}
        </List>
    );
}

const connectedRaces = connect()(Races);
export { connectedRaces as Races }