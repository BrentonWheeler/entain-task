export function racesReducer(state = {}, action) {
  switch(action.type) {
    case "GET_RACES": 
      return action.payload
  }

  return state;
}

// If this file gets bloated, pull out actions into into a sibling folder
export function getRaces (dispatch) {
  return fetch('https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=100')
    .then(response => response.json())
    .then(data => {
        dispatch({ type: 'GET_RACES', payload: data})
    })
}