import { racesReducer } from './races'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

export function rootReducer(state = {}, action) {
    return {
      races: racesReducer(state.races, action)
    }
}

export const store = createStore(rootReducer, applyMiddleware(thunk));